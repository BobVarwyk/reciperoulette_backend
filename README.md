######### THE PROJECT ###########

Recipe Roulette REST API is an API which enables the user to generate weekly recipe schedules based on their personal accounts linked to their allergies, diets and household size. 
It also offers the functionality to make grocery lists with the ingredients of the recipes.

######### THE DEPENDECIES ###########

Recipe Roulette REST API uses the following dependecies :

* spring-boot-starter-webflux
* gson
* spring-boot-starter-web
* spring-boot-starter-test
* persistence-api
* spring-data-jpa
* spring-boot-starter-data-mongodb
* passay
* springfox-swagger-ui
* spring-boot-starter-mail
* spring-boot-starter-thymeleaf
* commons-io

All these dependecies are needed for several functions within the API. Such as sending mail (spring-boot-starter-mail) and designing HTML like emails (spring-boot-starter-thymeleaf).

######### Database ###########

Recipe Roulette REST API uses MongoDB as it's datastorage. The API generates the collections itself. It is however required to create a database for  the REST API yourself. You can do so in tools like MongoDB 
Compass community.

######### Running the project ###########

Before running the code, you need to set some environment variables yourself :

* mongoDbHost - (Host adress of your Mongo database)
* mongoDbPort - (Host port of your Mongo database)
* Auth -(Authentication database of your MongoDB)
* mongoDbUser - (Username of MongoDB account)
* mongoDbPassword - (Password of MongoDB account)
* mongoDbDatabase - (Name of the mongoDB database you want to use)
* GmailUser - (Username of Gmail account from which to send emails)
* GmailPassword - (Password of Gmail account from which to send emails)
* GmailPort - (Port of the Gmail mailserver)
* spoonacularKey - (Spoonacular API key for the recipes)

after you've finished adding these variables, you can simply run the code in your editor or on your server as a Jar file

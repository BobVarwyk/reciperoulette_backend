package nl.idamediafoundry.reciperoulette.Account;

import nl.idamediafoundry.reciperoulette.Account.Repositories.AccountRepository;
import nl.idamediafoundry.reciperoulette.Groceries.Services.GroceryService;
import nl.idamediafoundry.reciperoulette.Recipes.Services.RecipeService;
import nl.idamediafoundry.reciperoulette.Responses;
import nl.idamediafoundry.reciperoulette.Account.Model.AccountToken;
import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Account.Requests.*;
import nl.idamediafoundry.reciperoulette.Account.Service.AccountTokenService;
import nl.idamediafoundry.reciperoulette.Account.Service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@CrossOrigin
@RestController
@Api(value="Account system", produces = "application/json")
public class AccountController {


    @ApiOperation("Get user credentials based on the ID given from the front-end application")
    @GetMapping("/account/{userID}")
    public Object getUser(@PathVariable ObjectId userID) throws JSONException {
        ObjectId userId = userID;
        // Get the user with the ID given by front-end app.
        User selectedUser = UserService.getUserBasedID(userId);
        if (selectedUser != null) {
            return new JSONObject(selectedUser.getAllUserInfo()).toString();
        }
        return Responses.Error("Error! No user found with these credentials.");
    }

    @ApiOperation("Update the users details based on the ID given by the front-end application")
    @PutMapping("/account/update/{userID}")
    public Object userUpdate(@PathVariable ObjectId userID, @Valid @RequestBody EditCredentials credentials, BindingResult bindingResult) throws JSONException {
        // Get user based on given userID
        User selectedUser = UserService.getUserBasedID(userID);

        // Check if selected user exists
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Check if email has a valid syntax
        if(bindingResult.hasErrors()) {
            return Responses.Error(bindingResult.getFieldError().getDefaultMessage());
        }

        // If no errors in previous checks, set new credentials and save user
        selectedUser.setFirstName(credentials.getFirstName());
        selectedUser.setLastName(credentials.getLastName());
        selectedUser.setEmail(credentials.getEmail());
        selectedUser.setHouseHoldSize(credentials.getHouseHoldSize());
        UserService.saveUser(selectedUser);
        return Responses.Succes("Your account has been updated.");
    }

    @ApiOperation("Set the recently registered user household size in the user registration process")
    @PutMapping("/account/household/{userID}")
    public Object storeHouseholdSize(@RequestBody User user, @PathVariable ObjectId userID) throws JSONException {
        //Get the user that just registered
        User selectedUser = UserService.getUserBasedID(userID);
        if (selectedUser != null) {
            selectedUser.setHouseHoldSize(user.getHouseHoldSize());
            UserService.saveUser(selectedUser);
            return Responses.Succes("Your household size has been updated.");
        }
        return Responses.Error("Error! We can't seem to update your account at this time, try again later.");
    }

    @ApiOperation("Update the user password if the user changes it in the front-end app or if he asks for a reset link")
    @PutMapping("/account/update/password/{userID}")
    public Object updatePassword(@PathVariable ObjectId userID, @Valid @RequestBody Password passwordCredentials, BindingResult bindingResult) throws JSONException {
        // Get user based on the ID given
        User selectedUser = UserService.getUserBasedID(userID);

        // Check if user exists
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Check if all fields are filled in.
        if(passwordCredentials.getPassword().equals("") || passwordCredentials.getOldPassword().equals("") || passwordCredentials.getPasswordConfirm().equals("")) {
            return Responses.Error("Please fill in all fields.");
        }

        // Check if old password matches current password of user
        if(!UserService.comparePasswordHash(passwordCredentials.getOldPassword(), selectedUser.getPassword())) {
            return Responses.Error("Your old password is incorrect.");
        }

        if(!passwordCredentials.getPasswordConfirm().equals(passwordCredentials.getPasswordConfirm())) {
            return Responses.Error("Error! New passwords don't match.");
        }

        // Check if password has the correct syntax
        if(bindingResult.hasErrors()) {
            return Responses.Error(bindingResult.getFieldError().getDefaultMessage());
        }

        // If no errors, set new password and Save user
        selectedUser.setPassword(UserService.encryptPassword(passwordCredentials.getPassword()));
        UserService.saveUser(selectedUser);
        return Responses.Succes("Your password has been changed.");
    }

    @ApiOperation("Delete the user from the database")
    @DeleteMapping("/account/delete/{userID}")
    public Object deleteUser(@RequestBody Password password, @PathVariable ObjectId userID) throws JSONException {
        // Get user based on userId
        User selectedUser = UserService.getUserBasedID(userID);

        // Check if user exists
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Delete recipe schedule linked to user
        if(RecipeService.scheduleExists(selectedUser)) {
            RecipeService.deleteSchedule(RecipeService.getAllUserRecipes(selectedUser));
        }

        // Delete grocery schedule linked to user
        if(GroceryService.getUserGroceryList(selectedUser.getUserId()) != null) {
            GroceryService.deleteGroceryList(selectedUser);
        }

        if(password.getPassword().equals("")) {
            return Responses.Error("Fill in your old password to proceed.");
        }

        // Compare password from Request body to password in database
        if(!UserService.comparePasswordHash(password.getPassword(), selectedUser.getPassword())) {
            return Responses.Error("Your password is incorrect.");
        }

        UserService.deleteUser(selectedUser);
        return Responses.Succes("Your account has been deleted.");
    }

    @ApiOperation("Resend the activation token by email")
    @PostMapping("/account/activate/resend")
    public Object resendbyEmail(@RequestBody User user) throws JSONException {
        boolean isSucces = false;
        String resultMessage = null;

        User selectedUser = UserService.getUserBasedEmail(user.getEmail());
        if(selectedUser != null) {
            AccountTokenService.createAccountToken(selectedUser.getEmail());
            isSucces = true;
            resultMessage = "Activation link has been resend to your email.";
        }
        if(selectedUser == null ) {
            isSucces = false;
            resultMessage = "Error! No user found with this email.";
        }
        return Responses.Standard(isSucces, resultMessage);
    }

    @ApiOperation("Resend the activation token by token ID")
    @PostMapping("/account/activate/resend/{token}")
    public Object resendbyToken(@PathVariable(name = "token") String token) throws JSONException {
        AccountToken selectedToken = AccountTokenService.tokenBasedID(token);
        if(selectedToken == null) {
            return Responses.Error("This activation link does not exist in our system.");
        }

        if(AccountTokenService.tokenIsValid(selectedToken)) {
            AccountTokenService.deleteAccountToken(selectedToken.getToken());
        }

        AccountTokenService.createAccountToken(selectedToken.getUser().getEmail());
        return Responses.Succes("The activation link has been resend to your email.");
    }

    @ApiOperation("Validation of the user account")
    @PutMapping("/account/activate/{token}")
    public Object activateAccount(@PathVariable String token) throws JSONException {
        // Get token based on token String given by user
        AccountToken selectedToken = AccountTokenService.tokenBasedID(token);
        // Check if token exists
        if(selectedToken == null) {
            return Responses.Error("This activation link does not exist in our system.");
        }

        // Check if selected token is a validation token and not a password reset token
        if(!selectedToken.getType().equals("activate")) {
            return Responses.Error("Error! this is a password reset token, not an activation token.");
        }
        // Check if activation token is still valid (24hour based)
        if(selectedToken.getExpireDate().isBefore(LocalDateTime.now())) {
            return Responses.Error("Error! This link has expired.");
        }

        // If the token is valid, set the user on activated
        User selectedUser = selectedToken.getUser();
        selectedUser.setIsVerified(true);
        UserService.saveUser(selectedUser);
        // Delete the activation token after usage
        AccountTokenService.deleteAccountToken(selectedToken.getToken());
        return Responses.Succes("Your account has been activated.");
    }

    @ApiOperation("Request a password reset link")
    @PostMapping(path = "/account/password/reset")
    public Object PasswordResetRequest(@RequestBody ResetRequest requestInfo) throws JSONException {
        // Get the email from request.
        String givenEmail = requestInfo.getEmail();
        // If email is empty, return function with error
        if(givenEmail.equals("")) {
            return Responses.Error("Please fill in your email to receive a reset link.");
        }
        // Find user based on the email given above.
        User selectedUser = UserService.getUserBasedEmail(givenEmail);
        // If user is not found, return function with error.
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        if(selectedUser.getIsVerified().equals(false)) {
            return Responses.Error("You need to activate your account first, check your mail for our activation link");
        }

        AccountTokenService.createPasswordResetToken(selectedUser.getEmail());
        return Responses.Succes("A recovery link has been send to your email.");
    }

    @ApiOperation("Reset the password by reset token")
    @PutMapping("/account/password/reset/{token}")
    public Object resetPassword(@PathVariable String token, @Valid @RequestBody Password passwordBody, BindingResult bindingResult) throws JSONException {
        AccountToken resetToken = AccountTokenService.tokenBasedID(token);
        // Check if token exists
        if(resetToken == null) {
            return Responses.Error("This link does not exist in our system.");
        }
        // Check if token is a password reset token
        if(!resetToken.getType().equals("reset")) {
            return Responses.Error("This is not an active reset token, please request a new one.");
        }

        // Check if token is still valid
        if(resetToken.getExpireDate().isBefore(LocalDateTime.now())) {
            return Responses.Error("This link has expired, please request a new one.");
        }

        // Get user linked to token
        User selectedUser = resetToken.getUser();
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        if(bindingResult.hasErrors()) {
            return Responses.Error(bindingResult.getFieldError().getDefaultMessage());
        }

        if(!passwordBody.getPassword().equals(passwordBody.getPasswordConfirm())) {
            return Responses.Error("Passwords don't match.");
        }

        selectedUser.setPassword(UserService.encryptPassword(passwordBody.getPassword()));
        UserService.saveUser(selectedUser);
        AccountTokenService.deleteAccountToken(resetToken.getToken());
        return Responses.Succes("Your password has been changed, you can now login with your new password.");
    }

    @ApiOperation("Get all allergies linked to signed in account.")
    @GetMapping("/allergies/{userID}")
    public Object userAllergies(@PathVariable ObjectId userID) throws JSONException {
        ObjectId userId = userID;
        // Get the user with the ID given by front-end app.
        User selectedUser = UserService.getUserBasedID(userId);
        if (selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        return Responses.Succes(selectedUser.getAllergies());
    }

    @ApiOperation("Set allergies on active or inactive")
    @PutMapping("/account/allergies/{userId}")
    public Object handleAllergies(@RequestBody Allergy allergy, @PathVariable ObjectId userId) throws JSONException {
        User selectedUser = UserService.getUserBasedID(userId);
        // Get the allergy name
        String selectedAllergy = allergy.getAllergyName();

        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        List<String> allergies = new ArrayList<>(Arrays.asList(selectedUser.getAllergies()));

        // If allergy is already active
        if(allergies.contains(selectedAllergy)) {
            allergies.remove(selectedAllergy);
        } else {
            allergies.add(selectedAllergy);
        }

        // Convert new list to string array for MongoDB save
        String[] newAllergies = allergies.toArray(new String[0]);
        // Save the updated allergies in MongoDB
        selectedUser.setAllergies(newAllergies);
        UserService.saveUser(selectedUser);
        return Responses.Succes(selectedUser.getAllergies());
    }

    @ApiOperation("Get all diets linked to signed in account.")
    @GetMapping("/diets/{userID}")
    public Object userDiets(@PathVariable ObjectId userID) throws JSONException {
        ObjectId userId = userID;
        // Get the user with the ID given by front-end app.
        User selectedUser = UserService.getUserBasedID(userId);
        if (selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        return Responses.Succes(selectedUser.getDiet());
    }

    @ApiOperation("Set diets on active or inactive")
    @PutMapping("/account/diets/{userId}")
    public Object handleDiets(@RequestBody Diet diet, @PathVariable ObjectId userId) throws JSONException {
        User selectedUser = UserService.getUserBasedID(userId);
        // Get the diet name
        String selectedDiet = diet.getDietName();

        if(selectedUser.equals(null)) {
            return Responses.Error("No user found with these credentials");
        }

        if(selectedDiet.equals(selectedUser.getDiet())) {
            selectedUser.setDiet("");
        } else {
            selectedUser.setDiet(selectedDiet);
        }

        UserService.saveUser(selectedUser);

        return Responses.Succes(selectedUser.getDiet());
    }
}

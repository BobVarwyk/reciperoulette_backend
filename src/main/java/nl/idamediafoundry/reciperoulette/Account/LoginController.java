package nl.idamediafoundry.reciperoulette.Account;

import nl.idamediafoundry.reciperoulette.Responses;
import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Account.Service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.json.JSONException;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/login", produces = "application/json")
@Api(value="Account login", produces = "Account login system")
public class LoginController {

    @ApiOperation("Login users if the user given credentials match with credentials in MongoDB")
    @PostMapping
    public Object userLogin(@RequestBody User body) throws JSONException {
        String email = body.getEmail().toLowerCase();
        String password = body.getPassword();

        // Check if both fields are filled in.
        if(email.equals("") && password.equals("")) {
            return Responses.Error("Please fill in all the fields.");
        }

        // Check if the user exists.
        User selectedUser = UserService.getUserBasedEmail(email);
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Check if the user is verified.
        if(selectedUser.getIsVerified().equals(false)) {
            return Responses.Error("Please active your account, We've send you a activation link after the registration proces.");
        }

        if(!UserService.comparePasswordHash(password, selectedUser.getPassword())) {
            return Responses.Error("Your password is incorrect.");
        }

        return Responses.Succes(selectedUser.getAllUserInfo());
    }
}

package nl.idamediafoundry.reciperoulette.Account.Model;

import org.bson.types.ObjectId;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class AccountToken {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private ObjectId id;

    @NotEmpty
    private String token;

    @NotEmpty
    private LocalDateTime expireDate;

    @NotEmpty
    private String type;

    @NotEmpty
    private  LocalDateTime issuedDate;

    @OneToOne
    @JoinColumn(name = "userId")
    private User user;

    public AccountToken() {
        this.token = UUID.randomUUID().toString();
        this.issuedDate = LocalDateTime.now();
        this.expireDate = LocalDateTime.now().plusHours(24);
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public LocalDateTime getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(LocalDateTime issuedDate) {
        this.issuedDate = issuedDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

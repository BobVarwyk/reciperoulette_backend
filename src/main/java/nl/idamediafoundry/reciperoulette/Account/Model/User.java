package nl.idamediafoundry.reciperoulette.Account.Model;

import nl.idamediafoundry.reciperoulette.Account.Constraints.ValidPassword;
import io.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private ObjectId userId;

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @Email
    @NotEmpty
    private String email;

    @NotEmpty
    @ValidPassword
    private String password;

    // Household size of the user
    private int houseHoldSize;

    private String[] allergies;

    private String diet = "";

    // Activated, default is false for registration.
    private Boolean isVerified = false;

    //Default constructor for login and such.
    public User() {
    }

    public User(ObjectId userId, String firstName, String lastName, String email, String password, String diet) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.diet = diet;
    }


    public ObjectId getUserId() {
        return userId;
    }

    @ApiModelProperty(hidden = true)
    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getHouseHoldSize() {
        return houseHoldSize;
    }

    public void setHouseHoldSize(int houseHoldSize) {
        this.houseHoldSize = houseHoldSize;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public String[] getAllergies() {
        return allergies;
    }

    public void setAllergies(String[] allergies) {
        this.allergies = allergies;
    }

    public String getDiet() {
        return diet;
    }

    public void setDiet(String diet) {
        this.diet = diet;
    }

    @ApiModelProperty(hidden = true)
    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }

    @ApiModelProperty(hidden = true)
    public Map getAllUserInfo() {
        Map<String, Object> userInfo = new HashMap<String, Object>();
        userInfo.put("id", this.getUserId().toString());
        userInfo.put("firstName", this.getFirstName());
        userInfo.put("lastName", this.getLastName());
        userInfo.put("email", this.getEmail());
        userInfo.put("allergies", this.getAllergies());
        userInfo.put("diets", this.getDiet());
        userInfo.put("householdSize", this.getHouseHoldSize());
        userInfo.put("activated", this.getIsVerified());
        return userInfo;
    }
}

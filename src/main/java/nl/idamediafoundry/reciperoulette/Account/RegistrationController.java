package nl.idamediafoundry.reciperoulette.Account;

import nl.idamediafoundry.reciperoulette.Responses;
import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Account.Service.AccountTokenService;
import nl.idamediafoundry.reciperoulette.Account.Service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping(value = "/register", produces = "application/json")
@Api(value="Account registration", produces = "Account registration system, for creation of accounts with personal profile.")
public class RegistrationController {

    @ApiOperation("Create user with user generated credentials")
    @PostMapping
    public Object userRegister(@Valid @RequestBody User user, BindingResult bindingResult) throws JSONException {
        Boolean isSucces;
        String resultMessage;

        if(!UserService.emailAlreadyExists(user.getEmail())) {
            isSucces = false;
            resultMessage = "This email is already linked to an account.";
        } else {
            user.setUserId(ObjectId.get());
            user.setPassword(UserService.encryptPassword(user.getPassword()));
            UserService.saveUser(user);
            // CREATE ACTIVATION TOKEN
            AccountTokenService.createAccountToken(user.getEmail());
            // RETURN SUCCES VALUE
            isSucces = true;
            resultMessage = "Account has been created";
        }

        if(bindingResult.hasErrors()) {
            isSucces = false;
            resultMessage = bindingResult.getFieldError().getDefaultMessage();
        }
        return Responses.Standard(isSucces, resultMessage);
    }
}

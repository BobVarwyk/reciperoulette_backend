package nl.idamediafoundry.reciperoulette.Account.Repositories;

import nl.idamediafoundry.reciperoulette.Account.Model.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends MongoRepository<User, String> {
    // Find user based on ID
    User findByUserId(ObjectId userId);
    // Find user based on Email
    User findByEmail(String email);
}

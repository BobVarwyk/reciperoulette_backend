package nl.idamediafoundry.reciperoulette.Account.Repositories;

import nl.idamediafoundry.reciperoulette.Account.Model.AccountToken;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountTokenRepository extends MongoRepository<AccountToken, String> {
    // Find token by email adres
    List<AccountToken> findByUserEmail(String email);
    // Find token by token
    List<AccountToken> findByToken(String token);
}

package nl.idamediafoundry.reciperoulette.Account.Requests;

import javax.validation.constraints.NotEmpty;

public class Allergy {

    @NotEmpty
    private String allergyName;

    public String getAllergyName() {
        return allergyName;
    }

    public void setAllergyName(String allergyName) {
        this.allergyName = allergyName;
    }
}

package nl.idamediafoundry.reciperoulette.Account.Requests;

import javax.validation.constraints.NotEmpty;

public class Diet {

    @NotEmpty
    private String dietName;

    public String getDietName() {
        return dietName;
    }

    public void setDietName(String dietName) {
        this.dietName = dietName;
    }
}

package nl.idamediafoundry.reciperoulette.Account.Requests;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class EditCredentials {

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @Email
    @NotEmpty
    private String email;

    // Household size of the user
    private int houseHoldSize;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getHouseHoldSize() {
        return houseHoldSize;
    }

    public void setHouseHoldSize(int houseHoldSize) {
        this.houseHoldSize = houseHoldSize;
    }
}

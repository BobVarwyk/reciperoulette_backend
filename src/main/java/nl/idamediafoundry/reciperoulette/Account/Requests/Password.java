package nl.idamediafoundry.reciperoulette.Account.Requests;

import nl.idamediafoundry.reciperoulette.Account.Constraints.ValidPassword;

import javax.validation.constraints.NotEmpty;

public class Password {

    // If regular password change, old password is needed for security reasons
    private String oldPassword;

    @NotEmpty
    @ValidPassword
    private String password;

    @ValidPassword
    private String passwordConfirm;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}

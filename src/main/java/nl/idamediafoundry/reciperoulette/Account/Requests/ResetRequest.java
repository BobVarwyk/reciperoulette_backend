package nl.idamediafoundry.reciperoulette.Account.Requests;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;


public class ResetRequest {

    @Email
    @NotEmpty
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

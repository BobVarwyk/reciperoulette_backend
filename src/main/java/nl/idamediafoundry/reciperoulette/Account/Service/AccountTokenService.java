package nl.idamediafoundry.reciperoulette.Account.Service;

import nl.idamediafoundry.reciperoulette.Account.Model.AccountToken;
import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Account.Repositories.AccountRepository;
import nl.idamediafoundry.reciperoulette.Account.Repositories.AccountTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AccountTokenService {

    private static AccountRepository accountRepository;
    private static AccountTokenRepository accountTokenRepository;
    private static MailService mailService;
    private static String activationURL;
    private static String passwordResetURL;

    @Autowired private AccountTokenService(AccountRepository accountRepository, AccountTokenRepository accountTokenRepository, MailService mailService, @Value("${Target.Host}") String host, @Value("${Target.Activation.Link}") String activationURL, @Value("${Target.PasswordReset.Link}") String passwordResetURL) {
        AccountTokenService.accountRepository = accountRepository;
        AccountTokenService.accountTokenRepository = accountTokenRepository;
        AccountTokenService.mailService = mailService;
        AccountTokenService.activationURL = host+activationURL;
        AccountTokenService.passwordResetURL = host+passwordResetURL;
    }

    // Get list of all tokens with given Token String
    public static AccountToken tokenBasedID(String token) {
       List<AccountToken> accountTokens = accountTokenRepository.findByToken(token);
       if(!accountTokens.isEmpty()) {
           AccountToken accountToken = accountTokens.get(0);
           return accountToken;
       }
       return null;
    }

    // Get list of all tokens with email linked to token
    public static AccountToken tokenBasedEmail(String email) {
        List<AccountToken> accountTokens = accountTokenRepository.findByUserEmail(email);
        if(!accountTokens.isEmpty()) {
            AccountToken accountToken = accountTokens.get(0);
            return accountToken;
        }
        return null;
    }

    // Return tokens list based on email linked to token
    public static List<AccountToken> tokenListBasedEmail(String email) {
        List<AccountToken> accountTokens = accountTokenRepository.findByUserEmail(email);
        return accountTokens;
    }

    // Return tokens list based on token String linked to token
    public static List<AccountToken> tokenListBasedToken(String token) {
        List<AccountToken> accountTokens = accountTokenRepository.findByToken(token);
        return accountTokens;
    }

    // Check if the token is still valid (expired or not)
    public static Boolean tokenIsValid(AccountToken token) {
        if(token.getExpireDate().isBefore(LocalDateTime.now())) {
            return true;
        }
        return false;
    }

    // CREATE A TOKEN
    public static void createAccountToken(String email) {
        User selectedUser = UserService.getUserBasedEmail(email);

        List<AccountToken> accountTokens = accountTokenRepository.findByUserEmail(selectedUser.getEmail());
        AccountToken accountToken = null;
        if(accountTokens.isEmpty()) {
            accountToken = new AccountToken();
            accountToken.setId(selectedUser.getUserId());
            accountToken.setType("activate");
            accountToken.setUser(selectedUser);
            accountTokenRepository.save(accountToken);
        } else {
            for(AccountToken token : accountTokens) {
                if(token.getType().equals("activate")) {
                    accountToken = token;
                }
            }
        }

        // Set the mail parameters
        String mailSubject = "Activate your account";
        String mailContent = "Welcome to Recipe Roulette, you can activate your account via the link in this mail.";
        String mailTargetLink = activationURL+accountToken.getToken();;
        // Create the mail with the Account token and activation link
        mailService.sendHtmlMail(email, mailSubject, mailContent, mailTargetLink, "welcome");
    }

    // CREATE PASSWORD RESET TOKEN
    public static void createPasswordResetToken(String email) {
        User selectedUser = accountRepository.findByEmail(email);

        List<AccountToken> accountTokens = accountTokenRepository.findByUserEmail(email);
        AccountToken accountToken = null;

        if(accountTokens.isEmpty()) {
            accountToken = new AccountToken();
            accountToken.setId(selectedUser.getUserId());
            accountToken.setType("reset");
            accountToken.setUser(selectedUser);
            accountTokenRepository.save(accountToken);
        } else {
            for(AccountToken token : accountTokens) {
                if(token.getType().equals("reset")) {
                    accountToken = token;
                }
            }
        }

        // Set the mail parameters
        String mailSubject = "Your password reset link";
        String mailContent = "By clicking on the link you can reset your password";
        String mailTargetLink = passwordResetURL+accountToken.getToken();;
        // Create the mail with the Account token and activation link
        mailService.sendHtmlMail(email, mailSubject, mailContent, mailTargetLink, "passwordReset");
    }

    // DELETE THE ACTIVATED TOKEN
    public static void deleteAccountToken(String tokenToDelete) {
        if(tokenToDelete == null) {
            throw new java.lang.Error("Het account token is niet gevonden in het systeem.");
        }
        List<AccountToken> accountTokens = accountTokenRepository.findByToken(tokenToDelete);
        AccountToken selectedToken = accountTokens.get(0);
        accountTokenRepository.delete(selectedToken);
    }
}

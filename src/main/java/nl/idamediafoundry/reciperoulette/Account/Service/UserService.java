package nl.idamediafoundry.reciperoulette.Account.Service;

import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Account.Repositories.AccountRepository;
import org.bson.types.ObjectId;
import org.passay.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class UserService {
    private static AccountRepository repository;

    // Add a private constructor so class can't be initialized
    private UserService() {}

    @Autowired
    public void UserService(AccountRepository repository) {UserService.repository = repository;}

    // Find user based on ID
    public static User getUserBasedID(ObjectId userId) {
        return repository.findByUserId(userId);
    }

    // Find user based on email
    public static User getUserBasedEmail(String mail) {
        return repository.findByEmail(mail);
    }

    // Save the user in the database
    public static User saveUser(User user) {
        return repository.save(user);
    }

    // Delete user from the database
    public static void deleteUser(User user) {
        repository.delete(user);
    }

    // Function to check if email already exists in database
    public static boolean emailAlreadyExists(String email) {
        if(repository.findByEmail(email) == null) {
            return true;
        }
        return false;
    }

    //Encrypt password given by the user
    public static String encryptPassword(String passwordToEncrypt) {
        BCryptPasswordEncoder userPassword = new BCryptPasswordEncoder();
        return userPassword.encode(passwordToEncrypt);
    }

    public static String checkPasswordValidation(String password) {
        PasswordValidator validator = new PasswordValidator(Arrays.asList(
                // at least 8 characters
                new LengthRule(8, 30),
                // at least one upper-case character
                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                // at least one lower-case character
                new CharacterRule(EnglishCharacterData.LowerCase, 1),
                // at least one digit character
                new CharacterRule(EnglishCharacterData.Digit, 1),
                // no whitespace
                new WhitespaceRule()
        ));

        RuleResult result = validator.validate(new PasswordData(password));
        String messageTemplate = String.join(",", validator.getMessages(result));
        return messageTemplate;
    }

    public static boolean comparePasswordHash(String passwordInput, String passwordHashed) {
        if(BCrypt.checkpw(passwordInput, passwordHashed)) {
            return true;
        }
        return false;
    }
}

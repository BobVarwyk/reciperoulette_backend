package nl.idamediafoundry.reciperoulette.Groceries;

import com.sun.mail.iap.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Account.Service.UserService;
import nl.idamediafoundry.reciperoulette.Groceries.Models.GroceriesList;
import nl.idamediafoundry.reciperoulette.Groceries.Requests.GroceryDayList;
import nl.idamediafoundry.reciperoulette.Groceries.Requests.GroceryItem;
import nl.idamediafoundry.reciperoulette.Groceries.Services.GroceryService;
import nl.idamediafoundry.reciperoulette.Recipes.Models.CompactRecipe;
import nl.idamediafoundry.reciperoulette.Recipes.Services.RecipeService;
import nl.idamediafoundry.reciperoulette.Responses;
import org.bson.types.ObjectId;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.naming.Binding;
import javax.validation.Valid;
import java.nio.channels.SelectableChannel;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@CrossOrigin
@RestController
@Api(value="Grocery system component", produces = "application/json")
public class GroceryController {

    @ApiOperation("Create a grocery list for the upcoming week for user.")
    @PostMapping("/groceries/{userId}/create")
    public Object createGroceryList(@PathVariable ObjectId userId){
        // Get user based on given userID on front-end
        User selectedUser = UserService.getUserBasedID(userId);

        // Check if selected user exists in MongoDB
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        GroceriesList selectedGroceries = GroceryService.getUserGroceryList(selectedUser.getUserId());

        // Check if selectGroceries is not empty and expired
        if(selectedGroceries != null && GroceryService.checkExpirationGroceryList(selectedGroceries)) {
            GroceryService.deleteGroceryList(selectedUser);
        }

        // Check if selectGroceries is not empty and not expired
        if(selectedGroceries != null && !GroceryService.checkExpirationGroceryList(selectedGroceries)) {
            return Responses.Error("Error! You seem to already have a grocery list for this week.");
        }

        // If made through all checks, create a new grocery list for the user.
        GroceryService.CreateGroceryList(selectedUser);
        return Responses.Succes("Congratulations, a new grocery list was generated for you.");
    }

    @ApiOperation("Update entire grocery list based on day name")
    @PutMapping("/groceries/{userId}/update")
    public Object UpdateGroceryList(@PathVariable ObjectId userId, @Valid @RequestBody GroceryDayList groceryList, BindingResult bindingResult) {
        // Get user based on given userID on front-end
        User selectedUser = UserService.getUserBasedID(userId);

        // Check if selected user exists in MongoDB
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Get all values from requestBody
        String day = groceryList.getDay();
        String[] groceries = groceryList.getGroceries();

        if(bindingResult.hasErrors()) {
            return Responses.Error("Your day " + bindingResult.getFieldError().getDefaultMessage());
        }

        // Get the grocery list of the selected user
        GroceriesList userGroceries = GroceryService.getUserGroceryList(selectedUser.getUserId());

        // Check if selected user has a grocery list in MongoDB
        if(userGroceries == null) {
            return Responses.Error("Error! this user doesn't have an active grocery list yet.");
        }

        // Update GroceryList based on day and new groceries.
        if(!GroceryService.updateGroceryList(userGroceries, day, groceries)) {
            return Responses.Error("Error! this day was not found in our system");
        }

        return Responses.Succes("Your grocery list has been updated.");
    }

    @ApiOperation("Delete the grocery list from a specific day")
    @DeleteMapping("/groceries/{userId}/delete/{day}")
    public Object deleteGroceriesByDay(@PathVariable ObjectId userId, @PathVariable String day) {
        // Get user based on given userID on front-end
        User selectedUser = UserService.getUserBasedID(userId);

        // Check if selected user exists in MongoDB
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Get the grocery list of the selected user
        GroceriesList userGroceries = GroceryService.getUserGroceryList(selectedUser.getUserId());

        // Check if selected user has a grocery list in MongoDB
        if(userGroceries == null) {
            return Responses.Error("Error! this user doesn't have an active grocery list yet.");
        }

        // Init empty array for groceries parameter
        String[] newGroceryValue = new String[]{};

        if(!GroceryService.updateGroceryList(userGroceries, day, newGroceryValue)) {
            return Responses.Error("Error! this day was not found in our system.");
        }

        return Responses.Succes("Your groceries for " + day + "have been deleted.");
    }

    @ApiOperation("Get all the groceries of the user, categorized by day")
    @GetMapping("/groceries/{userId}/get/all")
    public Object getAllGroceries(@PathVariable ObjectId userId) {
        User selectedUser = UserService.getUserBasedID(userId);

        // Check if selected user exists in MongoDB
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Get the grocery list of the selected user
        GroceriesList userGroceries = GroceryService.getUserGroceryList(selectedUser.getUserId());

        // Check if selected user has a grocery list in MongoDB
        if(userGroceries == null) {
            return Responses.Error("Error! this user doesn't have an active grocery list yet.");
        }

        if(GroceryService.checkExpirationGroceryList(userGroceries)) {
            return Responses.Error("Error! There is no active grocery list found, please generate a new recipe schedule to active your new grocery list.");
        }

        return Responses.Succes(userGroceries.getAllGroceriesPerDay());
    }

    @ApiOperation("Get groceries of one specific day")
    @GetMapping("/groceries/{userId}/get/{day}")
    public Object getGroceriesByDay(@PathVariable ObjectId userId, @PathVariable String day) {
        User selectedUser = UserService.getUserBasedID(userId);

        // Check if selected user exists in MongoDB
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Get the grocery list of the selected user
        GroceriesList userGroceries = GroceryService.getUserGroceryList(selectedUser.getUserId());

        // Check if the user has a active grocery list
        if(userGroceries == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Check if the grocery list isn't expired.
        if(GroceryService.checkExpirationGroceryList(userGroceries)) {
            return Responses.Error("Error! There is no active grocery list found, please generate a new recipe schedule to active your new grocery list.");
        }

        if(GroceryService.getGroceriesByDay(selectedUser, day) == null) {
            return Responses.Error("Error! this day was not found in our system.");
        }

        return Responses.Succes(GroceryService.getGroceriesByDay(selectedUser, day));
    }

    @ApiOperation("Set one specific grocery item based on the day and item.")
    @PutMapping("/groceries/{userId}/update/{day}")
    public Object updateSpecificGroceryItemByDay(@PathVariable ObjectId userId, @PathVariable String day, @RequestBody GroceryItem groceryItem) {
        User selectedUser = UserService.getUserBasedID(userId);

        // Check if user exists
        if(selectedUser == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Get the grocery list of the selected user
        GroceriesList userGroceries = GroceryService.getUserGroceryList(selectedUser.getUserId());

        // Check if grocery list exists
        if(userGroceries == null) {
            return Responses.Error("Error! No user found with these credentials.");
        }

        // Set the new list
        if(!GroceryService.updateGroceryItem(userGroceries, day, groceryItem.getGroceryItem())) {
            return Responses.Error("Error! We couldn't update your grocery list at this time.");
        }
        return Responses.Succes("Your groceries have been saved.");
    }
}

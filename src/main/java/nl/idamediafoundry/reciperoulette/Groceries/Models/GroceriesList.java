package nl.idamediafoundry.reciperoulette.Groceries.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import nl.idamediafoundry.reciperoulette.Account.Model.User;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class GroceriesList {

    @NotEmpty
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private ObjectId id;

    @NotEmpty
    private ObjectId userId;

    @NotEmpty
    private LocalDateTime creationDate;

    @NotEmpty
    private LocalDateTime expireDate;

    private String[] monday;

    private String[] tuesday;

    private String[] wednesday;

    private String[] thursday;

    private String[] friday;

    private String[] saturday;

    private String[] sunday;

    public GroceriesList(ObjectId userId) {
        this.userId = userId;
        this.creationDate = LocalDateTime.now();
        this.expireDate = LocalDateTime.now().plusDays(7);
        this.monday = new String[] {};
        this.tuesday = new String[] {};
        this.wednesday = new String[] {};
        this.thursday = new String[] {};
        this.friday = new String[] {};
        this.saturday = new String[] {};
        this.sunday = new String[] {};
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public String[] getMonday() {
        return monday;
    }

    public void setMonday(String[] monday) {
        this.monday = monday;
    }

    public String[] getTuesday() {
        return tuesday;
    }

    public void setTuesday(String[] tuesday) {
        this.tuesday = tuesday;
    }

    public String[] getWednesday() {
        return wednesday;
    }

    public void setWednesday(String[] wednesday) {
        this.wednesday = wednesday;
    }

    public String[] getThursday() {
        return thursday;
    }

    public void setThursday(String[] thursday) {
        this.thursday = thursday;
    }

    public String[] getFriday() {
        return friday;
    }

    public void setFriday(String[] friday) {
        this.friday = friday;
    }

    public String[] getSaturday() {
        return saturday;
    }

    public void setSaturday(String[] saturday) {
        this.saturday = saturday;
    }

    public String[] getSunday() {
        return sunday;
    }

    public void setSunday(String[] sunday) {
        this.sunday = sunday;
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public Map getAllGroceriesPerDay() {
        Map<String, Object> groceries = new HashMap<String, Object>();
        groceries.put("monday", this.getMonday());
        groceries.put("tuesday", this.getTuesday());
        groceries.put("wednesday", this.getWednesday());
        groceries.put("thursday", this.getThursday());
        groceries.put("friday", this.getFriday());
        groceries.put("saturday", this.getSaturday());
        groceries.put("sunday", this.getSunday());
        return groceries;
    }
}

package nl.idamediafoundry.reciperoulette.Groceries.Repositories;

import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Groceries.Models.GroceriesList;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GroceryListRepository extends MongoRepository<GroceriesList, String> {
    GroceriesList findByUserId(ObjectId userId);
}

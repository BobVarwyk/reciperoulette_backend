package nl.idamediafoundry.reciperoulette.Groceries.Requests;

import org.bson.types.ObjectId;

import javax.validation.constraints.NotEmpty;

public class GroceryDayList {

    @NotEmpty
    private String day;

    private String[] groceries;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String[] getGroceries() {
        return groceries;
    }

    public void setGroceries(String[] groceries) {
        this.groceries = groceries;
    }
}

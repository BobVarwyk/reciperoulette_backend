package nl.idamediafoundry.reciperoulette.Groceries.Requests;

import javax.validation.constraints.NotEmpty;

public class GroceryItem {

    @NotEmpty
    public String groceryItem;

    public String getGroceryItem() {
        return groceryItem;
    }

    public void setGroceryItem(String groceryItem) {
        this.groceryItem = groceryItem;
    }
}



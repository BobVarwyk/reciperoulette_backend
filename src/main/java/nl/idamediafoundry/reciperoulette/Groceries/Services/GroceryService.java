package nl.idamediafoundry.reciperoulette.Groceries.Services;

import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Groceries.Models.GroceriesList;
import nl.idamediafoundry.reciperoulette.Groceries.Repositories.GroceryListRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Service
public class GroceryService {

    // Init grocerylist repository to create, update and delete documents in MongoDB
    private static GroceryListRepository groceryListRepository;

    private GroceryService(GroceryListRepository groceryListRepository) {
        GroceryService.groceryListRepository = groceryListRepository;
    }

    // Create new Grocery list for upcoming week and save it in MongoDB
    public static void CreateGroceryList(User user) {
        // Init a new Grocery list from GroceriesList POJO
        GroceriesList userGroceries = new GroceriesList(user.getUserId());
        groceryListRepository.save(userGroceries);
    }

    // Check if the user already has a grocery list active
    public static GroceriesList getUserGroceryList(ObjectId userId) {
        GroceriesList userGroceries = groceryListRepository.findByUserId(userId);
        if(userGroceries == null) {
            return null;
        }
        return userGroceries;
    }

    // Check if the grocery list of user is not expired
    public static Boolean checkExpirationGroceryList(GroceriesList groceries) {
        if(groceries.getExpireDate().isBefore(LocalDateTime.now())) {
            // It's expired
            return true;
        }
        // It's not expired
        return false;
    }

    // Delete the grocery list from the database if it's expired or not necessary.
    public static void deleteGroceryList(User user) {
        GroceriesList listToDelete = groceryListRepository.findByUserId(user.getUserId());
        groceryListRepository.delete(listToDelete);
    }

    // Update the grocery list based on day given by the front-end.
    public static Boolean updateGroceryList(GroceriesList groceryList, String day, String[] groceries) {
        // Check which day to update based on day given by front-end requestBody
        switch (day) {
            case "monday":
                groceryList.setMonday(groceries);
                break;
            case "tuesday":
                groceryList.setTuesday(groceries);
                break;
            case "wednesday":
                groceryList.setWednesday(groceries);
                break;
            case "thursday":
                groceryList.setThursday(groceries);
                break;
            case "friday":
                groceryList.setFriday(groceries);
                break;
            case "saturday":
                groceryList.setSaturday(groceries);
                break;
            case "sunday":
                groceryList.setSunday(groceries);
                break;
            default:
                System.out.println("No matching day found");
                return false;
        }

        // Save the updated groceryList in MongoDB
        groceryListRepository.save(groceryList);
        return true;
    }

    public static String[] getGroceriesByDay(User user, String day) {
        // Get the schedule from the given user
        GroceriesList userGroceries = getUserGroceryList(user.getUserId());

        // Init the return value
        String[] result;

        // Check which day to update based on day given by front-end requestBody
        switch (day) {
            case "maandag":
                result = userGroceries.getMonday();
                break;
            case "dinsdag":
                result = userGroceries.getTuesday();
                break;
            case "woensdag":
                result = userGroceries.getWednesday();
                break;
            case "donderdag":
                result = userGroceries.getThursday();
                break;
            case "vrijdag":
                result = userGroceries.getFriday();
                break;
            case "zaterdag":
                result = userGroceries.getSaturday();
                break;
            case "zondag":
                result = userGroceries.getSunday();
                break;
            default:
                System.out.println("No matching day found");
                return null;
        }
        return result;
    }

    public static Boolean updateGroceryItem(GroceriesList groceriesList, String day, String groceryItem) {
        // Check which day to update based on day given by front-end requestBody
        switch (day) {
            case "monday":
                groceriesList.setMonday(setNewGroceries(groceriesList.getMonday(), groceryItem));
                break;
            case "tuesday":
                groceriesList.setTuesday(setNewGroceries(groceriesList.getTuesday(), groceryItem));
                break;
            case "wednesday":
                groceriesList.setWednesday(setNewGroceries(groceriesList.getWednesday(), groceryItem));
                break;
            case "thursday":
                groceriesList.setThursday(setNewGroceries(groceriesList.getThursday(), groceryItem));
                break;
            case "friday":
                groceriesList.setFriday(setNewGroceries(groceriesList.getFriday(), groceryItem));
                break;
            case "saturday":
                groceriesList.setSaturday(setNewGroceries(groceriesList.getSaturday(), groceryItem));
                break;
            case "sunday":
                groceriesList.setSunday(setNewGroceries(groceriesList.getSunday(), groceryItem));
                break;
            default:
                System.out.println("No matching day found");
                return false;
        }

        // Save the groceriesList to the database
        groceryListRepository.save(groceriesList);
        return true;
    }

    // Use this function for setting new groceries per day
    private static String[] setNewGroceries(String[] currentGroceries, String selectedGrocery) {
        // Init the return value
        String[] result = new String[0];
        // Convert the currentgroceries array to a list to use contains, add and remove
        List<String> oldGroceriesList = new LinkedList<String>(Arrays.asList(currentGroceries));

        if(oldGroceriesList.contains(selectedGrocery)) {
            // Delete grocery item from the list
            oldGroceriesList.remove(selectedGrocery);
        } else {
            // Add grocery item to the list
            oldGroceriesList.add(selectedGrocery);
        }

        // Convert list back to String array
        result = oldGroceriesList.toArray(new String[0]);

        // Return the new arrayList
        return result;
    }
}

package nl.idamediafoundry.reciperoulette.Recipes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Account.Service.UserService;
import nl.idamediafoundry.reciperoulette.Recipes.Models.Favorites;
import nl.idamediafoundry.reciperoulette.Recipes.Services.FavoriteService;
import nl.idamediafoundry.reciperoulette.Responses;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@Api(value="Favorites", produces = "application/json")
public class FavoriteController {

    @ApiOperation("Create a new favorite list for user.")
    @PostMapping("/favorites/create/{userId}")
    public Object CreateFavoritesList(@PathVariable ObjectId userId) {
        // Check if user exists
        User selectedUser = UserService.getUserBasedID(userId);
        if(selectedUser == null) {
            return Responses.Error("No users found with these credentials");
        }

        // Create the favorites list, FavoritesService checks if the user exits
        if(!FavoriteService.createFavoritesList(selectedUser.getUserId())) {
            return Responses.Error("This user already has a list of their favorites recipes.");
        }
        return Responses.Succes("Favorites list for " + selectedUser.getFirstName() + " has been created");
    }

    @ApiOperation("Handle adding and removing recipes from favorites")
    @PutMapping("/{userId}/favorites/handle/{recipeId}")
    public Object handleFavorite(@PathVariable ObjectId userId, @PathVariable String recipeId) {
        // Check if user exists
        User selectedUser = UserService.getUserBasedID(userId);
        if(selectedUser == null) {
            return Responses.Error("No users found with these credentials");
        }

        // Check if user already has an active favorites list, if not... create one.
        if(!FavoriteService.userHasFavoriteList(selectedUser.getUserId())) {
            FavoriteService.createFavoritesList(selectedUser.getUserId());
        }

        // Add or remove the recipes from the list and store it in a new list
        List<String> newFavoriteList =  FavoriteService.handleFavorites(selectedUser.getUserId(), recipeId);

        // Save the new list in the Mongo database
        FavoriteService.saveFavorites(selectedUser.getUserId(), newFavoriteList);
        return Responses.Succes("Your favorites have been updated");
    }
}

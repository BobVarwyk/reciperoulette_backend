package nl.idamediafoundry.reciperoulette.Recipes.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;

// Recipe model for MongoDB (Not allowed to save anything other than ID in database)
public class CompactRecipe {

    @NotEmpty
    @JsonProperty("id")
    private String id;

    @NotEmpty
    private String dayName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }
}

package nl.idamediafoundry.reciperoulette.Recipes.Models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Favorites {

    @NotEmpty
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private ObjectId id;

    @NotEmpty
    private ObjectId userId;

    private List<String> recipes = new ArrayList<>();

    public Favorites(ObjectId userId) {
        this.userId = userId;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public List<String> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<String> recipes) {
        this.recipes = recipes;
    }
}

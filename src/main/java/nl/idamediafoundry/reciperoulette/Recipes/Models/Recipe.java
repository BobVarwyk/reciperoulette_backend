package nl.idamediafoundry.reciperoulette.Recipes.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Recipe {

    @NotEmpty
    private String id;

    @NotEmpty
    private String title;

    @NotEmpty
    private String[] dishTypes;

    @NotEmpty
    private String readyInMinutes;

    @NotEmpty
    private String servings;

    @NotEmpty
    private String image;

    private List<Object> extendedIngredients;

    private List<Object> nutrients;

    private List<Object> instructions;

    private String dayName;

    public Recipe(String id, String title, String[] dishTypes, String readyInMinutes,  String servings, String image, List<Object> extendedIngredients, List<Object> nutrients, List<Object> instructions) {
        this.id = id;
        this.title = title;
        this.dishTypes = dishTypes;
        this.readyInMinutes = readyInMinutes;
        this.servings = servings;
        this.image = image;
        this.extendedIngredients = extendedIngredients;
        this.nutrients = nutrients;
        this.instructions = instructions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getDishTypes() {
        return dishTypes;
    }

    public void setDishTypes(String[] dishTypes) {
        this.dishTypes = dishTypes;
    }

    public String getReadyInMinutes() {
        return readyInMinutes;
    }

    public void setReadyInMinutes(String readyInMinutes) {
        this.readyInMinutes = readyInMinutes;
    }

    public String getServings() {
        return servings;
    }

    public void setServings(String servings) {
        this.servings = servings;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Object> getExtendedIngredients() {
        return extendedIngredients;
    }

    public void setExtendedIngredients(List<Object> extendedIngredients) {
        this.extendedIngredients = extendedIngredients;
    }

    public List<Object> getNutrients() {
        return nutrients;
    }

    public void setNutrients(List<Object> nutrients) {
        this.nutrients = nutrients;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public List<Object> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<Object> instructions) {
        this.instructions = instructions;
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public Map getAllRecipeData() {
        Map<String, Object> recipeInfo = new HashMap<String, Object>();
        recipeInfo.put("id", this.getId());
        recipeInfo.put("title", this.getTitle());
        recipeInfo.put("dishType", this.getDishTypes());
        recipeInfo.put("servingTime", this.getReadyInMinutes());
        recipeInfo.put("servings", this.getServings());
        recipeInfo.put("image", this.getImage());
        recipeInfo.put("ingredients", this.getExtendedIngredients());
        recipeInfo.put("nutrients", this.getNutrients());
        recipeInfo.put("instructions", this.getInstructions());
        recipeInfo.put("day", this.getDayName());
        return recipeInfo;
    }
}

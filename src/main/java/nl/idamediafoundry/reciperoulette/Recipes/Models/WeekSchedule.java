package nl.idamediafoundry.reciperoulette.Recipes.Models;

import nl.idamediafoundry.reciperoulette.Account.Model.User;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.List;

public class WeekSchedule {

    @NotEmpty
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private ObjectId id;

    @NotEmpty
    private LocalDateTime generatedDate;

    @NotEmpty
    private LocalDateTime expireDate;

    @NotEmpty
    private ObjectId userId;

    @NotEmpty
    private List<CompactRecipe> recipes;

    public WeekSchedule() {
        this.generatedDate = LocalDateTime.now();
        this.expireDate = LocalDateTime.now().plusDays(7);
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public LocalDateTime getGeneratedDate() {
        return generatedDate;
    }

    public void setGeneratedDate(LocalDateTime generatedDate) {
        this.generatedDate = generatedDate;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public List<CompactRecipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<CompactRecipe> recipes) {
        this.recipes = recipes;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }
}

package nl.idamediafoundry.reciperoulette.Recipes.Repositories;

import nl.idamediafoundry.reciperoulette.Recipes.Models.Favorites;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FavoritesRepository extends MongoRepository<Favorites, String> {
    // Find favorites based on the ID of the user.
    Favorites findByUserId(ObjectId userId);
}

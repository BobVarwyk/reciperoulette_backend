package nl.idamediafoundry.reciperoulette.Recipes.Repositories;

import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Recipes.Models.WeekSchedule;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface WeekScheduleRepository extends MongoRepository<WeekSchedule, String> {
    // Find single schedule by id of user
    WeekSchedule findByUserId(ObjectId userId);
}

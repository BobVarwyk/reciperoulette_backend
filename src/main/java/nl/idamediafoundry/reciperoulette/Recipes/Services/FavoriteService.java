package nl.idamediafoundry.reciperoulette.Recipes.Services;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.idamediafoundry.reciperoulette.Recipes.Models.Favorites;
import nl.idamediafoundry.reciperoulette.Recipes.Repositories.FavoritesRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class FavoriteService {

    private static FavoritesRepository favoritesRepository;

    // Create a private constructor so class can't be initialized
    private FavoriteService(FavoritesRepository favoritesRepository) {
        FavoriteService.favoritesRepository = favoritesRepository;
    }

    // Create favorites list
    public static boolean createFavoritesList(ObjectId userId) {
        if(favoritesRepository.findByUserId(userId) != null) {
            return false;
        }
        // Save the new favorites list in the Mongo database
        Favorites newFavoritesList = new Favorites(userId);
        favoritesRepository.save(newFavoritesList);
        return true;
    }

    // Check if the user already has a favorites list
    public static boolean userHasFavoriteList(ObjectId userId) {
        if(favoritesRepository.findByUserId(userId) != null) {
            return true;
        }
        return false;
    }

    public static List handleFavorites(ObjectId userId, String recipeId) {
        List<String> userFavorites = favoritesRepository.findByUserId(userId).getRecipes();
        // Check if list already contains the recipeId element
        if(userFavorites.contains(recipeId)) {
            userFavorites.remove(recipeId);
        } else {
            userFavorites.add(recipeId);
        }
        return userFavorites;
    }

    public static void saveFavorites(ObjectId userId , List<String> newFavoritesList) {
        Favorites newFavorites = favoritesRepository.findByUserId(userId);
        newFavorites.setRecipes(newFavoritesList);
        try {
            favoritesRepository.save(newFavorites);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

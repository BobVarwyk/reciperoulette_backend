package nl.idamediafoundry.reciperoulette.Recipes.Services;

import com.google.gson.Gson;

import nl.idamediafoundry.reciperoulette.Account.Model.User;
import nl.idamediafoundry.reciperoulette.Recipes.Models.CompactRecipe;
import nl.idamediafoundry.reciperoulette.Recipes.Models.Recipe;
import nl.idamediafoundry.reciperoulette.Recipes.Models.WeekSchedule;
import nl.idamediafoundry.reciperoulette.Recipes.Repositories.WeekScheduleRepository;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

import java.net.URL;
import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class RecipeService {

    private static WeekScheduleRepository weekScheduleRepository;

    // API KEY FOR SPOONACULAR, SEE PROPERTIES FILE FOR API
    private static String spoonacularApiKey;
    // Init webclient to use in all functions that require a API call
    private static WebClient webClient = WebClient.create();
    // Init GSON instance
    private static Gson gsonResponse = new Gson();

    // Empty private constructor to prevent other classes from initializing this service.
    private RecipeService(WeekScheduleRepository weekScheduleRepository, @Value("${Spoonacular.API.key}") String spoonacularApi) {
        RecipeService.weekScheduleRepository = weekScheduleRepository;
        RecipeService.spoonacularApiKey = spoonacularApi;
    }

    // Generate 7 recipes based on User preferences
    public static CompactRecipe[] GenerateWeeklyRecipes(User user) {
        // Get the user details (allergies & diets) for recipe outcome.
        String allergies = String.join(",", user.getAllergies());
        String diet = user.getDiet();

        // Init the empty recipes[] POJO
        CompactRecipe recipes[] = null;

        // Init the response string of API call
        String response = "";
        try {
            // Execute call and store result in String
            response = webClient.get()
                    .uri(uriBuilder -> uriBuilder.scheme("https").host("api.spoonacular.com")
                            .path("recipes/complexSearch")
                            .queryParam("number", 7)
                            .queryParam("type", "main course")
                            .queryParam("intolerances", allergies)
                            .queryParam("instructionsRequired", true)
                            .queryParam("diet", diet)
                            .queryParam("apiKey", spoonacularApiKey)
                            .queryParam("sort", "random").build())
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();
        } catch(WebClientException ex) {
            System.out.println(ex.getLocalizedMessage());
            return null;
        }

        // Get Results array from response and convert it to a GSON objects which fills the Recipe POJO
        try {
            JSONObject jsonResponse = new JSONObject(response);
            String json = jsonResponse.getString("results");
            recipes = gsonResponse.fromJson(json, CompactRecipe[].class);

            // Init LocalDate
            LocalDate localdate = LocalDate.now();
            // Add day name to recipe object
            for(CompactRecipe recipe : recipes) {
                recipe.setDayName(localdate.format(DateTimeFormatter.ofPattern("EEEE", Locale.ENGLISH)));
                localdate = localdate.plusDays(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        // Return the recipes list, whether empty or filled.
        return recipes;
    }

    public static void createWeeklySchedule(User user, List<CompactRecipe> recipes) {
        WeekSchedule newUserSchedule = new WeekSchedule();
        newUserSchedule.setUserId(user.getUserId());
        newUserSchedule.setRecipes(recipes);

        // Save schedule in MongoDB
        weekScheduleRepository.save(newUserSchedule);
    }

    public static Boolean scheduleExists(User user) {
        WeekSchedule selectedSchedule = weekScheduleRepository.findByUserId(user.getUserId());
        if(selectedSchedule == null) {
            return false;
        }
        return true;
    }

    public static WeekSchedule getAllUserRecipes(User user) {
        return weekScheduleRepository.findByUserId(user.getUserId());
    }

    public static Boolean userAlreadyHasValidSchedule(User user) {
        if(weekScheduleRepository.findByUserId(user.getUserId()) == null) {
            return false;
        }

        WeekSchedule selectedSchedule = weekScheduleRepository.findByUserId(user.getUserId());
        if(selectedSchedule.getExpireDate().isBefore(LocalDateTime.now())) {
            deleteSchedule(selectedSchedule);
            return true;
        }
        return false;
    }

    public static Boolean isScheduleExpired(User user) {
        WeekSchedule selectedSchedule = weekScheduleRepository.findByUserId(user.getUserId());

        if(selectedSchedule.getExpireDate().isBefore(LocalDateTime.now())) {
            return true;
        }
        return false;
    }

    public static void deleteSchedule(WeekSchedule schedule) {
        // Delete the schedule
        weekScheduleRepository.delete(schedule);
    }

    public static Boolean dayExists(String dayName) {
        // Init automatic List with weekdays from DateFormatSymbols class
        Locale Dutch = new Locale("nl", "NL");
        List<String> dayNames = Arrays.asList(DateFormatSymbols.getInstance(Dutch).getWeekdays());
        // Check if given day by API call exists
        if(dayNames.contains(dayName)) {
            return true;
        }
        return false;
    }

    public static String getRecipeDayName(User user, String id) {
        // Get the matching schedule
        WeekSchedule selectedWeekSchedule = weekScheduleRepository.findByUserId(user.getUserId());

        // Check if selectedWeekschedule is null, if so return log error and return null to end function
        if(selectedWeekSchedule == null) {
            System.out.println("Geen recepten schema gevonden");
            return null;
        }

        // Init return String
        String selectedDay = "";

        // Fill return String with dayName of recipe
        for(CompactRecipe recipe : selectedWeekSchedule.getRecipes()) {
            if(recipe.getId().equals(id)) {
                selectedDay = recipe.getDayName();
            }
        }
        // Return selectedDay String
        return selectedDay;
    }

    // Get basic Recipe information from weekschedule of user
    public static List<Object> getAllScheduleRecipes(User user) throws JSONException {
        // Init list to return at end of function
        List<Object> returnList = new ArrayList<>();

        // Get week schedule of given user
        WeekSchedule userSchedule = weekScheduleRepository.findByUserId(user.getUserId());

        // Init empty String array for the ID's of recipes
        ArrayList<String> recipeIds = new ArrayList<>();

        // Loop through week schedule of user, store id's of recipes in String array
        for(CompactRecipe compactRecipe : userSchedule.getRecipes()) {
            recipeIds.add(compactRecipe.getId());
        }

        // Convert String array to comma separated String for Spoonacular Call parameter
        String recipeId = String.join(",", recipeIds);

        // Init the response string of API call
        String response = "";
        try {
            // Execute Spoonacular API call to get all recipe information
            response = webClient.get()
                    .uri(uriBuilder -> uriBuilder.scheme("https").host("api.spoonacular.com")
                            .path("recipes/informationBulk")
                            .queryParam("ids", recipeId)
                            .queryParam("includeNutrition", true)
                            .queryParam("apiKey", spoonacularApiKey).build())
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();
        } catch(WebClientException ex) {
            System.out.println(ex.getLocalizedMessage());
            return null;
        }

        try {
            // Convert response String to JSONArray
            JSONArray jsonResponse = new JSONArray(response.trim().replaceAll(" +", " "));

            // Iterate through all recipes in the jsonResponse array
            for(int i = 0; i < jsonResponse.length(); i++) {
                // Get the JSON object
                JSONObject recipeFromAPI = jsonResponse.getJSONObject(i);

                // Get the data needed to fill the Recipe model
                String id = recipeFromAPI.getString("id");
                String title = recipeFromAPI.getString("title");
                String[] dishTypes = gsonResponse.fromJson(recipeFromAPI.getJSONArray("dishTypes").toString(), String[].class);
                String readyInMinutes = recipeFromAPI.getString("readyInMinutes");
                String servings = recipeFromAPI.getString("servings");

                // Convert image to Base64 encode for offline images
                // byte[] imageContent = IOUtils.toByteArray(new URL(recipeFromAPI.getString("image")));
                // String base64EncodedImage = Base64.getEncoder().encodeToString(imageContent);
                String image = recipeFromAPI.getString("image");

                List ingredients = gsonResponse.fromJson(recipeFromAPI.getJSONArray("extendedIngredients").toString(), List.class);
                List nutrients = gsonResponse.fromJson(recipeFromAPI.getJSONObject("nutrition").getString("nutrients"), List.class);
                List instructions = gsonResponse.fromJson(recipeFromAPI.getJSONArray("analyzedInstructions").toString(), List.class);
                String dayName = getRecipeDayName(user, id);

                // Fill the recipe POJO with the information from the API
                Recipe recipe = new Recipe(id, title, dishTypes, readyInMinutes, servings, image, ingredients, nutrients, instructions);
                recipe.setDayName(dayName);
                // Add all recipe data to the list which is returned by this function
                returnList.add(recipe.getAllRecipeData());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return returnList;
    }

    public static Recipe getSingleRecipeInfo(String id) {
        // Init empty Recipe POJO for return value
        Recipe recipe = null;

        String response = "";
        try {
            // Execute Spoonacular API call
            response = webClient.get()
                    .uri(uriBuilder -> uriBuilder.scheme("https").host("api.spoonacular.com")
                            .path("recipes/" + id + "/information")
                            .queryParam("includeNutrition", true)
                            .queryParam("apiKey", spoonacularApiKey).build())
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();
        } catch(WebClientException ex) {
            System.out.println(ex.getLocalizedMessage());
            return null;
        }

        try {
            // Get the JSON object
            JSONObject recipeFromAPI = new JSONObject(response);

            // Get the data needed to fill the Recipe model
            String recipeId = recipeFromAPI.getString("id");
            String title = recipeFromAPI.getString("title");
            String[] dishTypes = gsonResponse.fromJson(recipeFromAPI.getJSONArray("dishTypes").toString(), String[].class);
            String readyInMinutes = recipeFromAPI.getString("readyInMinutes");
            String servings = recipeFromAPI.getString("servings");

            // Convert image to Base64 encode for offline images
            // byte[] imageContent = IOUtils.toByteArray(new URL(recipeFromAPI.getString("image")));
            // String base64EncodedImage = Base64.getEncoder().encodeToString(imageContent);
            String image = recipeFromAPI.getString("image");

            List ingredients = gsonResponse.fromJson(recipeFromAPI.getJSONArray("extendedIngredients").toString(), List.class);
            List nutrients = gsonResponse.fromJson(recipeFromAPI.getJSONObject("nutrition").getString("nutrients"), List.class);
            List instructions = gsonResponse.fromJson(recipeFromAPI.getJSONArray("analyzedInstructions").toString(), List.class);

            // Fill the recipe POJO with the information from the API
            recipe = new Recipe(recipeId, title, dishTypes, readyInMinutes, servings, image, ingredients, nutrients, instructions);

        } catch(Exception e) {
            System.out.println(e);
        }

        return recipe;
    }
}
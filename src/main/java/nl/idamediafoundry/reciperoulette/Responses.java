package nl.idamediafoundry.reciperoulette;

import nl.idamediafoundry.reciperoulette.Recipes.Models.Recipe;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Responses {

    public static String Standard(Boolean status, Object message) {
        Map<String, Object> errorMap = new HashMap<String, Object>();
        errorMap.put("message", message);
        errorMap.put("succes", status);
        return new JSONObject(errorMap).toString();
    }

    public static String Error(Object message) {
        Map<String, Object> errorMap = new HashMap<String, Object>();
        errorMap.put("message", message);
        errorMap.put("succes", false);
        return new JSONObject(errorMap).toString();
    }

    public static String Succes(Object message) {
        Map<String, Object> succesMap = new HashMap<String, Object>();
        succesMap.put("message", message);
        succesMap.put("succes", true);
        return new JSONObject(succesMap).toString();
    }

    public static String scheduleResponse(Object message, LocalDateTime expireDate) {
        Map<String, Object> scheduleMap = new HashMap<String, Object>();
        scheduleMap.put("message", message);
        scheduleMap.put("expireDate", expireDate);
        scheduleMap.put("succes", true);
        return new JSONObject(scheduleMap).toString();
    }
}
